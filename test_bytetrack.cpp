// #include <iostream>
#include <memory>
#include "BYTETracker.h"

int main()
{
    BYTETracker byte_track{};
    
    int x1[] = {5, 10, 15, 20, 25, 30, 35, 40};
    int y1[] = {5, 10, 15, 20, 25, 30, 35, 40};

    NvObject obj;
    vector<STrack> track_list;
    for(int i = 0; i < 100; i++)
    {
        vector<NvObject> nvObjects;
        
        obj.prob = 0.5;
        obj.label = 1;
        obj.rect[0] = 10;
        obj.rect[1] = 10;
        obj.rect[2] = 500;
        obj.rect[3] = 500;
        
        nvObjects.push_back(obj);
        
        obj.prob = 0.5;
        obj.label = 1;
        obj.rect[0] = 1000 + i;
        obj.rect[1] = 1000 + i;
        obj.rect[2] = 500;
        obj.rect[3] = 500;
        
        nvObjects.push_back(obj);

        track_list = byte_track.update(nvObjects);

        // std::cout << track_list.size() << endl;

        for(int t=0; t<int(track_list.size()); t++)
        {
            int track_id = track_list[t].track_id;
            std::cout << track_id << std::endl;
        }

    }
    
    return 0;
}