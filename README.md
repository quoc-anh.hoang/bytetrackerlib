# ByteTrackerLib



## How to build

- clone this repo
- mkdir build
- cd build
- cmake ..
- make 

## Examples

Example code in "test_bytetrack.cpp".

**Step 1**: Init the tracker by using "BYTETracker byte_track{};", the input of "byte_track.update()" is the NvObjects (line 16).

**Step 2**: Each obj in NvObjects has 6 parameters: 

- obj.prob: confidence of object detection
- obj.label: class of object detection
- obj.rect[4]: [x1, y1, width, height]

**Step 3**: The output is track_list, each track has track_id, bbox (tlwh),... (can be found in includes/STrack.h)
